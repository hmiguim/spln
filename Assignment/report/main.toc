\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {portuguese}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Processamento de Linguagem Natural}{2}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Povoamento do Corpora}{2}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}An\IeC {\'a}lise do corpora}{4}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}M\IeC {\'o}dulo de Capitaliza\IeC {\c c}\IeC {\~a}o Preferencial}{4}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {new}}{4}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {process}}{5}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {preference}}{5}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {replaceNonGreedy}}{6}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {replaceGreedy}}{6}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {asTxt}}{6}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}M\IeC {\'o}dulo de Estat\IeC {\'\i }sticas}{6}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {new}}{7}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {add}}{7}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {info}}{7}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {absoluteFrequency}}{7}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {relativeFrequency}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {absoluteFrequencies}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {relativeFrequencies}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {frequencies}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {sum}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {subtract}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {surprise}}{8}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {threshold}}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {top}}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {bottom}}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {cleanData}}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {saveAsHtml}}{9}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}M\IeC {\'o}dulo de An\IeC {\'a}lise}{10}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {new}}{10}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {words}}{10}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {findEntities}}{11}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {findAcronyms}}{11}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sub-rotina \emph {classifier}}{12}{subsection.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Discuss\IeC {\~a}o dos Resultados}{13}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Conclus\IeC {\~o}es}{17}{chapter.3}
