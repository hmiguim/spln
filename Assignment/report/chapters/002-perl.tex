\chapter[Processamento de Linguagem Natural]{Processamento de Linguagem\\Natural}

Neste capítulo será explicado todo o processo, desde o povoamento do corpora de linguagem natural, em português, até à criação das scripts em Perl para a análise desse mesmo corpora. 

\section{Povoamento do Corpora}
\label{chp_2_sec_1_populate}

O povoamento do corpora foi realizado através da extração do conteúdo de notícias provenientes de um \textit{feed}. O \textit{feed} em questão é do Diário Digital e foram extraídas as notícias dos seguintes temas:

\begin{itemize}
	\item Auto-digital;
	\item Desporto;
	\item Lusofonia;
	\item Multimédia;
	\item Pessoas;
	\item Política;
	\item Saúde;
	\item Sociedade.
\end{itemize}  

Para o efeito, foi criada uma script em Perl capaz de extrair, limpar e armazenar continuamente num ficheiro de texto as diversas entradas extraídas do \textit{feed}. Através dos diversos \textit{feeds} de notícias foi possível extrair as seguintes informações: 

\begin{itemize}
	\item Título;
	\item URL;
	\item Data da publicação;
	\item Breve descrição.
\end{itemize} 

Apesar de ser possível a criação de um corpora bastante interessante com apenas os elementos acima referidos, o volume não era o desejável para uma análise gramatical e léxica, e produzir resultados palpáveis. Assim sendo, através da URL da notícia, a script em Perl é capaz de obter a página completa da notícia, e extrair o conteúdo noticioso completo, de forma a, engrossar o corpora. 

O ficheiro resultante encontra-se no formato XML, com os diferentes campos bem assinalados e delimitados. Em relação à alimentação do ficheiro por parte da script em Perl, esta é realizada de uma forma incremental. Na figura \ref{fig:chp_2_fig_exerto_do_corpora} é possível observar um exemplo de uma entrada do ficheiro XML em que, apesar de não se encontrar indentado, são visíveis os diferentes elementos XML presentes. Logo, para formar um corpora bem definido, é necessário criar o elemento XML pai \emph{news} com os seguintes filhos: \emph{title}, \emph{description}, \emph{txt}, \emph{date} e \emph{link}. 

\begin{figure}[hb]
\centering
\includegraphics[scale=0.55]{excerto_do_dataset.png}
\caption{Exemplo de uma entrada do ficheiro XML correspondente ao corpora.}
\label{fig:chp_2_fig_exerto_do_corpora}
\end{figure}

O elemento \emph{news} possui dois atributos: \emph{id} e \emph{category}. O atributo \emph{id} funciona como identificador para garantir que todas as notícias são entradas únicas. O valor que o \emph{id} toma provém da URL da notícia. O atributo \emph{category} serve para enriquecer o corpora e melhorar a análise. Os restantes elementos XML são auto explicativos, à exceção do atributo \emph{txt} que identifica o corpo da notícia. 

\section{Análise do corpora}
\label{chp_2_sec_1_analyze}

Para a análise do corpora foram criados três módulos distintos para o efeito, um módulo para a recapitalização preferencial, um módulo para a análise e um módulo para acomodar as estatísticas. Cada um dos módulos será explicado mais detalhadamente nas próximas subsecções deste trabalho prático.

\subsection{Módulo de Capitalização Preferencial}
\label{chp2_sec_1_subsec_1_pref_cap}

Este módulo é responsável por calcular o número de vezes que uma determinada palavra aparece capitalizada ou não e inferir a sua capitalização preferencial, para além disso, é dotada de métodos capazes de modificar o texto de acordo com a capitalização preferencial das palavras, de uma forma moderada ou agressiva. Para isso, o módulo em Perl possui as seguintes sub-rotinas: 

\begin{itemize}
	\item \textbf{\emph{new}} - responsável por inicializar a estrutura;
	\item \textbf{\emph{process}} - responsável por decompor o corpora em átomos e guardar registo do número de vezes que cada palavra aparece na forma capitalizada e não capitalizada;
	\item \textbf{\emph{preference}} - dada uma palavra devolve a sua capitalização preferencial, caso esta exista;
	\item \textbf{\emph{replaceNonGreedy}} - responsável por substituir as palavras pela sua capitalização preferencial de uma forma moderada;
	\item \textbf{\emph{replaceGreedy}} - responsável por substituir todas as palavras (agressiva) pela sua capitalização preferencial;
	\item \textbf{\emph{asTxt}} - devolve em formato CSV um ficheiro com a capitalização preferencial para cada palavra.
\end{itemize}
\setcounter{secnumdepth}{0}
\subsubsection{Sub-rotina \emph{new}}

Esta sub-rotina é responsável por inicializar a estrutura que vai acolher toda a informação referente à capitalização de cada palavra. É também nesta sub-rotina que o dicionário em português é inicializado. Este é oferecido através do módulo Perl Lingua::Jspell. O dicionário é usado mais tarde, como iremos ver, de forma a averiguar lexicalmente uma determinada palavra.  

\subsubsection{Sub-rotina \emph{process}}

A sub-rotina \emph{process}, recebe como argumento um texto e a partir do mesmo é feita uma divisão por palavras (contanto também as palavras hifenizadas). O resultado é guardado numa \textit{hash}, tendo como chave a palavra em minúscula e o seu valor é, por sua vez, outra \textit{hash} com as chaves a serem a palavra maiúscula ou minúscula e o número total de cada ocorrência. Por exemplo, a palavra ``a'' aparece no texto 25 vezes a maiúscula e 10 vezes a minúscula, a \textit{hash} resultante seria algo do género:

\begin{verbatim}
                "a" => { "A" => 25, "a" => 10 }
\end{verbatim}
Criada a estrutura com todas as palavras e o número de ocorrências das respetivas capitalizações, segue um processo de cálculo da capitalização preferencial para cada palavra presente na estrutura de dados. Este cálculo é feito através da comparação logarítmica entre os dois valores presentes, mas se palavra ocorrer apenas em minúscula ou em maiúscula, a diferença logarítmica não tem efeito e a escolha recai na capitalização que a palavra se encontra. Por exemplo, no caso anterior, seria impossível inferir a preferência, uma vez que, a diferença entre os logaritmos de ambas as palavras é menor que o valor 0,5. Para haver uma certeza, a diferença terá que ser superior a 0,5. Caso haja, uma clara afirmação que a palavra é preferencialmente maiúscula ou minúscula esta é guardada numa estrutura de dados para posterior consulta. Caso contrário, isto é, não exista uma clara afirmação da capitalização, são aplicadas uma série de heurísticas para tentar perceber, de certa forma, qual a preferência que essa palavra toma. Para isto, o módulo Lingua::Jspell é usado para determinar, por exemplo, se alguma dessas palavras indecisas, são verbos, caso sejam a sua capitalização preferencial deve ser sempre minúscula.

\subsubsection{Sub-rotina \emph{preference}}

Esta sub-rotina recebe como argumento uma palavra e devolve, caso exista, a sua correspondente em maiúscula ou minúscula, dependendo do cálculo realizado previamente. Esta sub-rotina apenas devolve o valor fixado na estrutura de dados após a sub-rotina \emph{process} ter sido executada.  

\subsubsection{Sub-rotina \emph{replaceNonGreedy}}

Esta sub-rotina tem como objetivo, substituir as palavras de início de frase pela sua correspondente em maiúscula ou minúscula. Caso não existe uma preferência concreta a palavra mantém tal como estava no texto original.

\subsubsection{Sub-rotina \emph{replaceGreedy}}

Esta sub-rotina surge como alternativa à sub-rotina \emph{replaceNonGreedy}. Ao contrário da última, a substituição das palavras não é feita apenas no início de cada frase, mas sim globalmente ao texto passado como argumento.

\subsubsection{Sub-rotina \emph{asTxt}}

Esta sub-rotina exporta em formato CSV um ficheiro com as capitalizações preferenciais para cada palavra presente no texto processado. 

\setcounter{secnumdepth}{2}

\subsection{Módulo de Estatísticas}
\label{chp2_sec_1_subsec_2_stats}
Este módulo é responsável por acomodar as estatísticas e calcular frequências absolutas, relativas ou logarítmicas para um determinado conjunto de dados, entre outros métodos com relevância estatística para o projeto. Para isso, o módulo em Perl possui as seguintes sub-rotinas: 

\begin{itemize}
	\item \textbf{\emph{new}} - responsável por inicializar a estrutura;
	\item \textbf{\emph{add}} - responsável adicionar uma entrada à estrutura de dados;
	\item \textbf{\emph{info}} - responsável por calcular o número de palavras únicas e o número de palavras no total presentes na estrutura de dados;
	\item \textbf{\emph{absoluteFrequency}} - dada uma palavra devolve a sua frequência absoluta;
	\item \textbf{\emph{relativeFrequency}} - dada uma palavra devolve a sua frequência relativa;
	\item \textbf{\emph{relativeFrequencies}} - responsável por calcular as frequências relativas para cada palavra na estrutura de dados;
	\item \textbf{\emph{absoluteFrequencies}} - responsável por calcular as frequências absolutas para cada palavra na estrutura de dados;
	\item \textbf{\emph{frequencies}} - responsável por calcular as frequências absolutas, relativos e logarítmicas para cada palavra na estrutura de dados; 
	\item \textbf{\emph{sum}} - responsável por somar as frequências absolutas de dois ou mais conjuntos estatísticos;
	\item \textbf{\emph{subtract}} - responsável por subtrair as frequências absolutas de dois conjuntos estatísticos; 
	\item \textbf{\emph{surprise}} - dado dois conjuntos A e B calcula as entradas que caracterizam o conjunto A; 
	\item \textbf{\emph{threshold}} - responsável por devolver todas as entradas de um conjunto que tenham mais que k ocorrências;
	\item \textbf{\emph{top}} - dado um argumento numérico retorna as n primeiras entradas com maior frequência absoluta; 
	\item \textbf{\emph{bottom}} - dado um argumento numérico retorna as n primeiras entradas com menor frequência absoluta; 
	\item \textbf{\emph{cleanData}} - responsável por eliminar todos os registos das estruturas de dados;
	\item \textbf{\emph{saveAsHtml}} - responsável por exportar em formato HTML um ficheiro com as estatísticas pretendidas.
\end{itemize}
\setcounter{secnumdepth}{0}

\subsubsection{Sub-rotina \emph{new}}

Esta sub-rotina é responsável por inicializar a estrutura que vai acolher toda a informação referente às palavras e as suas ocorrências. 

\subsubsection{Sub-rotina \emph{add}}

Esta sub-rotina adiciona uma entrada à estrutura de dados, caso a entrada já exista na estrutura, o seu valor é incrementado.

\subsubsection{Sub-rotina \emph{info}}

Para uma dada estrutura de dados não vazia, calcula o número único de entradas e o número total de entradas na estrutura de dados.

\subsubsection{Sub-rotina \emph{absoluteFrequency}}

A partir de uma palavra passada como argumento, esta rotina é capaz de calcular a frequência absoluta da mesma.

\subsubsection{Sub-rotina \emph{relativeFrequency}}

A partir de uma palavra passada como argumento, esta rotina é capaz de calcular a frequência relativa da mesma. As frequências relativas calculadas são feitas por permilagem, ao invés de percentagem. 

\subsubsection{Sub-rotina \emph{absoluteFrequencies}}

Esta sub-rotina é responsável por calcular a frequência absoluta de cada palavra presente na estrutura de dados.

\subsubsection{Sub-rotina \emph{relativeFrequencies}}

Esta sub-rotina é responsável por calcular a frequência relativa de cada palavra presente na estrutura de dados. Tal como visto anteriormente, as frequências relativas calculadas são feitas por permilagem, ao invés de percentagem. 

\subsubsection{Sub-rotina \emph{frequencies}}

Esta sub-rotina é mais completa que as anteriores, no que diz respeito ao cálculo das frequências, uma vez que, agrega numa nova e única estrutura de dados, as frequências absolutas, relativas (por milhão) e logarítmicas, de cada palavra presente na estrutura de dados do conteúdo original.

\subsubsection{Sub-rotina \emph{sum}}

Esta sub-rotina calcula o somatório das frequências absolutas de dois ou mais conjuntos passados como argumentos, devolvendo uma nova estrutura acomodando as novas estatísticas calculadas.

\subsubsection{Sub-rotina \emph{subtract}}

Através desta sub-rotina é possível subtrair dois conjuntos estatísticos. Caso a subtração de uma entrada dê um valor negativo a entrada não é adicionada na nova estrutura de dados. Por exemplo, subtrair os conjuntos \emph{A} e \emph{B}, em que \emph{A} possui uma entrada \emph{k} com 1 ocorrência e \emph{B} possui uma entrada \emph{k} com 3 ocorrências, a subtração para a entrada \emph{k} será igual a -2, e por sua vez não transitará para a nova estrutura. 

\subsubsection{Sub-rotina \emph{surprise}}

Esta sub rotina recebe como argumento dois conjuntos estatísticos e um valor \emph{k} criterioso para determinar se uma entrada poderá ser considerada classificadora de um conjunto estatístico. Esta rotina funciona essencialmente através da comparação da frequência logarítmica entre a mesma entrada dos dois conjuntos. Caso a diferença seja superior a um determinado \emph{k}, que por defeito está definido como sendo de 0.5, a entrada é considerada estatisticamente representativa daquele conjunto.

\subsubsection{Sub-rotina \emph{threshold}}

Dado um determinado argumento numérico retorna as \emph{n} entradas com frequências absolutas superiores a \emph{k} ocorrências. Finda a seleção, os valores para as frequências relativas e logarítmicas são recalculados face às novas entradas selecionadas.

\subsubsection{Sub-rotina \emph{top}}

Através de um argumento numérico retorna os n primeiros elementos ordenados de forma decrescente de frequências absolutas. Caso nenhum argumento seja passado à sub-rotina esta retorna os 10 primeiros elementos com maior frequência absoluta. Caso de empate os registos são ordenados alfabeticamente. Os valores para as frequências relativas e logarítmicas são recalculados face às novas entradas selecionadas.

\subsubsection{Sub-rotina \emph{bottom}}

Através de um argumento numérico retorna os n primeiros elementos ordenados de forma ascendente de frequências absolutas. Caso nenhum argumento seja passado à sub-rotina esta retorna os 10 primeiros elementos com menor frequência absoluta. Caso de empate os registos são ordenados alfabeticamente. Os valores para as frequências relativas e logarítmicas são recalculados face às novas entradas selecionadas.

\subsubsection{Sub-rotina \emph{cleanData}}

Esta sub-rotina limpa a estruturas de dados, eliminando todos os registos contidos na mesma.

\subsubsection{Sub-rotina \emph{saveAsHtml}}

Esta sub-rotina exporta em formato HTML um ficheiro com as estatísticas que foram solicitadas, isto é, esta rotina pode ser encadeada com qualquer das sub-rotinas apresentadas nesta secção (\ref{chp2_sec_1_subsec_2_stats}). Pode receber dois argumentos, o tipo de ordenação (ascendente ou descendente), e o nome do ficheiro a exportar. Caso não sejam fornecidos os dois argumentos, por defeito o ficheiro será exportado em ordem descendente com o nome a ser composto pela data e hora da geração. 

\setcounter{secnumdepth}{2}
\subsection{Módulo de Análise}
\label{chp2_sec_1_subsec_2_analyze}

O módulo de análise possui as rotinas necessárias para descobrir e detetar entidades ou acrónimos.
Possui também uma rotina capaz de inferir que palavras estão relacionadas com um determinado tema, um classificador. Para isso, o módulo em Perl possui as seguintes sub-rotinas: 

\begin{itemize}
	\item \textbf{\emph{new}} - responsável por inicializar a estrutura;
	\item \textbf{\emph{words}} - responsável por calcular o número de palavras presentes no texto;
	\item \textbf{\emph{findEntities}} - responsável por identificar as entidades;
	\item \textbf{\emph{findAcronyms}} - responsável por identificar os acrónimos;
	\item \textbf{\emph{classifier}} - responsável por identificar as palavras que identificam um determinado tema.
\end{itemize}
\setcounter{secnumdepth}{0}
\subsubsection{Sub-rotina \emph{new}}

Sub-rotina responsável por inicializar as estruturas de dados que vão acolher os diferentes dados a armazenar pelas outras sub-rotinas presentes no módulo. Neste módulo é também utilizado o dicionário disponibilizado pelo módulo Lingua::Jspell, e por isso inicializado nesta sub-rotina. A presença do dicionário neste módulo será, como iremos ver, explicada em maior detalhe aquando da descrição das outras sub-rotinas presentes no módulo de análise. 

\subsubsection{Sub-rotina \emph{words}}

A sub-rotina \emph{words} recebe como argumento um texto e retorna estatísticas sobre o texto, através da utilização das sub-rotinas presentes no módulo de estatísticas, previamente apresentado. A partir da execução desta sub-rotina é possível inquirir o número de palavras presentes no texto, as frequências de cada palavra ou de uma determinada palavra.

\subsubsection{Sub-rotina \emph{findEntities}}

Esta sub-rotina, tal como a anterior - \emph{words} - faz uso do módulo de estatísticas. Para executar a mesma, é necessário passar como argumento um texto. A deteção das entidades é feita através de uma expressão regular composta. 
\begin{verbatim}
        1) my $pm 	= qr{[[:upper:]]\w+};
        2) my $prep = qr{d[eoa]s?};
        3) my $np   = qr{$pm(( )?(-)?($prep )?$pm)*};
\end{verbatim} 
A expressão regular 1) deteta um conjunto de caracteres alfanuméricos, em que o primeiro carácter é uma maiúscula. A expressão regular 2) apanha as proposições de, do, da, dos, das. A expressão regular 3) é a composição das duas primeiras expressões regulares para formar a expressão regular para a deteção de entidades. Com a expressão regular 3) é possível detetar grande parte das entidades presentes no texto, desde entidades como Pinto da Costa, Presidente da República, Procuradoria-Geral da República, Portugal, Vila Nova de Famalicão, entre outras. Portanto, neste caso, a regra usada para identificar uma palavra, ou conjunto de palavras como sendo uma entidade, foi a seguinte, esta tinha que ser composta por uma ou mais palavras em maiúscula, ligadas, ou não, por uma proposição ou então, separadas por um espaço ou hífen. Contudo, é ainda realizada uma operação para verificar se o padrão detetado pela expressão regular é uma entidade. Essa operação é realizada de uma forma um pouco rudimentar, seguindo os seguintes passos: caso o número de palavras seja superior a 1 é marcado como uma entidade válida, caso o número de palavras seja igual a 1, o dicionário é usado, para perceber se a palavra em questão tem como categoria nome próprio. Caso não seja uma entidade validada, o padrão detetado é adicionado a uma quarentena para ser tratado posteriormente.  

\subsubsection{Sub-rotina \emph{findAcronyms}}

A deteção de acrónimos no texto usa também um conjunto de expressões regulares para a procura das mesmas. É relativamente simples fazer a pesquisa de um acrónimo. O padrão geral passa por um conjunto de palavras começadas por letra maiúscula seguida de um palavra toda capitalizada entre parênteses, por exemplo, Serviço Nacional de Saúde (SNS) é um acrónimo válido. Para tal foram usadas as seguintes expressões regulares:   
\begin{verbatim}
1) my $pn = qr{([[:upper:]]\w+)};
2) my $prepositions = qr{(d[eoa]s?|para|em|sobre|[àaeo]s?)};
3) my $acro = qr{($pn(( $prepositions)?(-|\s)$pn)+) (\([A-Z]+\))};  
\end{verbatim} 
Em relação à sub-rotina anterior - \emph{findEntities} - foram reutilizadas as expressões regulares 1) e 2), com uma pequena adição de proposições na expressão regular 2). A expressão regular 3) deteta então todos os acrónimos que estejam de acordo com o padrão geral acima referido, um conjunto de palavras começadas por letra maiúscula seguidos de uma palavra entre parênteses. Contudo, para casos como, por exemplo, Procuradoria-Geral da República serem detetados pela expressão regular, esta tem que detetar palavras separadas não só por um espaço, mas também por um hífen. Além dos padrões terem que fazer correspondência com a expressão regular, existe um processo de verificação para confirmar que se trata, de facto, de um acrónimo válido. O processo começa por, verificar se o número de palavras presentes na versão por extenso do acrónimo (sem as proposições) corresponde ao número de letras que o acrónimo tem. Por exemplo, Autoridade de Segurança Alimentar e Económica (ASAE), retirando as preposições, contém quatro palavras, para passar no primeiro teste, o acrónimo tem que obrigatoriamente ter quatro letras. De seguida, é verificado se cada letra em cada posição no acrónimo corresponde, de facto, à mesma primeira letra da palavra na mesma posição na versão extensa do acrónimo. Por exemplo, em Autoridade de Segurança Alimentar e Económica (ASAE), é verificado se a primeira palavra, Autoridade, começa pela mesma letra, A, no acrónimo, e esta verificação é feita de igual forma para as restantes palavras. Neste caso, o padrão passava e era assumido como sendo um acrónimo válido. Caso não seja validade, o mesmo é armazenado numa quarentena para ser tratado posteriormente.

\subsubsection{Sub-rotina \emph{classifier}}

Esta sub-rotina devolve um conjunto de palavras que, possivelmente, são candidatas a identificar certos temas. O processo é feito da seguinte forma, primeiramente, para cada notícia é identificado o tema em que esta está inserida, depois são identificadas as diferentes palavras presentes nessa notícia. Identificado o tema, as palavras são guardadas numa estrutura de dados separadas por tema, à semelhança com a rotina \emph{words} o processo é análogo. Composta a estrutura de dados para cada tema, segue um processo de comparação entre os conjuntos de palavras. Primeiramente é calculado o conjunto comum através da soma das frequências absolutas por tema. Depois para cada tema, a rotina \emph{surprise} do módulo de estatística é usada e como argumento são fornecidos o conjunto relativo ao tema e o conjunto comum. A rotina irá devolver todas as palavras que se destaquem do conjunto comum, essa mesmo são consideradas identificadores candidatos para um determinado tema. 

\setcounter{secnumdepth}{2}
\section{Discussão dos Resultados}

Recolhidas as notícias dos \textit{feeds} pela script em Perl responsável, seguiu-se uma análise ao texto. 
O corpora recolhido conta com 1 276 177 palavras, com notícias recolhidas entre 19 de março de 2016 e 19 de maio de 2016, sendo que as dez mais comuns podem ser consultadas na figura \ref{fig:chp_2_fig_top_10}. 

\begin{figure}[hb]
\includegraphics[scale=0.40]{top10.png}
\caption{Top 10 das palavras mais frequentes no corpora analisado.}
\label{fig:chp_2_fig_top_10}
\end{figure}

Fazendo uso do módulo de capitalização preferencial (\ref{chp2_sec_1_subsec_1_pref_cap}), previamente explicado, o corpora recolhido é analisado para a construção da tabela de capitalização preferencial. Construída a tabela, através da rotina \emph{process} presente no módulo, o corpora é substituído, tanto pelo modo agressivo como pelo modo moderado e, através do módulo de análise (\ref{chp2_sec_1_subsec_2_analyze}), os textos são analisados através das rotinas \emph{findEntities} e \emph{findAcronyms}. Na tabela \ref{table:chp_2_tab_greedy_non_greedy_comparison_entities} e na tabela \ref{table:chp_2_tab_greedy_non_greedy_comparison_acronyms} podemos observar uma comparação entre os dois tipos de substituição e como afeta o número de entidades de acrónimos identificados através das rotinas acima referidas. 
\newpage

\begin{table}[htp]
\centering
\caption{Comparação entre o método de substituição moderada e agressiva na identificação das entidades.}
\begin{tabular}{ccc}

\textbf{Substituição} & \textbf{Entidades Distintas} & \textbf{Total de Entidades} \\
\hline
Moderada	& 16 097 & 72 359 \\
Agressiva	& 13 351 & 68 477 \\ 

\hline
\end{tabular}
\label{table:chp_2_tab_greedy_non_greedy_comparison_entities}
\end{table} 

Como é possível observar, na tabela \ref{table:chp_2_tab_greedy_non_greedy_comparison_entities}, uma substituição moderada, permite identificar mais três mil entidades distintas em relação a uma substituição mais agressiva. No total de entidades a substituição moderada permite um ganho de mais quatro mil entidades.

\begin{table}[htp]
\centering
\caption{Comparação entre o método de substituição moderada e agressiva na identificação dos acrónimos.}
\begin{tabular}{ccc}

\textbf{Substituição} & \textbf{Acrónimos Distintos} & \textbf{Total de Acrónimos} \\
\hline
Moderada	& 562 & 1 539 \\
Agressiva	& 221 & 810 \\ 

\hline
\end{tabular}
\label{table:chp_2_tab_greedy_non_greedy_comparison_acronyms}
\end{table} 

Relativamente aos acrónimos, na tabela \ref{table:chp_2_tab_greedy_non_greedy_comparison_acronyms}, a mesma conclusão mantém-se, fazer uma substituição moderada ao invés de uma substituição mais agressiva, permite angariar mais acrónimos. De uma forma agressiva foram identificados 810 acrónimos sendo que apenas 221 são distintos. Por outro lado, a substituição moderada permite identificar 562 acrónimos distintos num total de 1539 acrónimos. 

A discrepância dos resultados pode ser explicada com um simples exemplo, o acrónimo Produto Interno Bruto (PIB) é detetado quando usada a substituição moderada e inexistente quando usada a substituição agressiva. Como a substituição agressiva acontece globalmente no texto, e sendo que a preferência para a palavra \emph{produto} é minúscula, logo o resultado final será produto Interno Bruto (PIB). Para rotina \emph{findAcronyms} é impossível detetar o acrónimo no fim da substituição de modo agressivo, visto que, com a expressão regular usada irá apenas fazer a correspondência com Interno Bruto (PIB), e através das verificações existentes na rotina não será considerado um acrónimo válido e consequentemente descartado.
\newpage
Fazendo uso da rotina \emph{classifier} presente no módulo de análise (\ref{chp2_sec_1_subsec_2_analyze}) combinada com a rotina \emph{threshold} do módulo de estatísticas (\ref{chp2_sec_1_subsec_2_stats}), foram selecionadas para cada tema, as palavras candidatas a identificar determinado tema cuja sua ocorrência, isto é, a sua frequência absoluta esteja acima dos 50 acontecimentos. Na tabela \ref{table:chp_2_tab_classifier_stats} podem ser consultadas o número distinto e total de palavras por cada tema.  

\begin{table}[htp]
\centering
\caption{Estatísticas sobre os identificadores candidatos de acordo com os diferentes temas.}
\begin{tabular}{lcc}

\textbf{Tema} & \textbf{Palavras Distintas} & \textbf{Total de Palavras} \\
\hline
Desporto & 182 & 26970 \\
Política & 75 & 12845 \\
Lusofonia & 69 & 8325 \\
Saúde & 34 & 4086 \\
Auto-digital & 29 & 2449 \\
Pessoas & 28 & 2544 \\
Sociedade & 26 & 2477 \\
Multimédia & 14 & 949 \\
\hline
\end{tabular}
\label{table:chp_2_tab_classifier_stats}
\end{table} 

Relativamente aos números apresentados na tabela, é de salientar a quantidade de palavras únicas capazes de identificar os temas de desporto, política e lusofonia em relação aos restantes temas. Contudo entre o top 3, o tema desporto leva uma clara vantagem no número de palavras distintas e no total de palavras, em grande parte devido ao número de artigos sobre desporto ser superior aos restantes.

As tabelas \ref{table:chp_2_top_5_sports}, \ref{table:chp_2_top_5_politics} e \ref{table:chp_2_top_5_luso} dizem respeito às 5 palavras mais usadas em cada um dos top 3 temas. Acabam por ser as palavras mais representativas para classificar uma determinada notícia. As palavras candidatas a identificar os temas podem ser consultadas no seguinte sítio: \url{https://goo.gl/PvvJ57}. 


\begin{table}[h]
\centering
\caption{Top 5 das palavras mais frequentes para o tema desporto.}
\begin{tabular}{lccc}

\textbf{Palavra} & \textbf{Freq. Absoluta} & \textbf{Freq. Relativa} & \textbf{Freq. Logarítmica} \\
\hline
jogo 		& 919 & 3.40748		& 0.53243 \\
liga 		& 847 & 3.14052		& 0.49700 \\
futebol 	& 803 & 2.97738		& 0.47383 \\
minutos 	& 734 & 2.72154		& 0.43481 \\
benfica 	& 725 & 2.68817		& 0.42945 \\
\hline
\end{tabular}
\label{table:chp_2_top_5_sports}
\end{table} 

Para a tabela \ref{table:chp_2_top_5_sports}, de facto, a cinco palavras mais usadas estão relacionadas com o tema de desporto. 

\begin{table}[!htbp]
\centering
\caption{Top 5 das palavras mais frequentes para o tema política.}
\begin{tabular}{lccc}

\textbf{Palavra} & \textbf{Freq. Absoluta} & \textbf{Freq. Relativa} & \textbf{Freq. Logarítmica} \\
\hline
psd					&	790		&	6.15025	&	0.78889	\\
república			&	720		&	5.60529	&	0.74859	\\
marcelo				&	500		&	3.89256	&	0.59023	\\
sousa				&	481		&	3.74464	&	0.57341	\\
ps					&	452		&	3.51887	&	0.54640	\\
\hline
\end{tabular}
\label{table:chp_2_top_5_politics}
\end{table} 

Em relação ao tema política (tabela \ref{table:chp_2_top_5_politics}), as palavras PSD, PS e república, estão de facto, relacionadas com o tema. Mas Marcelo e Sousa podem causar ambiguidades, já que, apesar de serem palavras pertencentes ao nome do atual presidente da república, aquando da redação deste relatório, podem levar à identificação do tema de uma notícia erradamente. Apesar disso, no atual contexto, a presença de Marcelo e Sousa no mesmo texto é um bom indicador que esse mesmo está relacionado com o tema política.  

\begin{table}[!htbp]
\centering
\caption{Top 5 das palavras mais frequentes para o tema lusofonia.}
\begin{tabular}{lccc}

\textbf{Palavra} & \textbf{Freq. Absoluta} & \textbf{Freq. Relativa} & \textbf{Freq. Logarítmica} \\
\hline
dilma		& 437 &	5.24924 &	0.7200 \\
rousseff	& 365 &	4.38438 &	0.6419 \\
partido		& 358 &	4.30030 &	0.6334 \\
brasil		& 344 &	4.13213 &	0.6161 \\
processo	& 269 &	3.23123 &	0.5093 \\
\hline
\end{tabular}
\label{table:chp_2_top_5_luso}
\end{table} 

Na tabela \ref{table:chp_2_top_5_luso} é possível consultar as palavras candidatas a identificar o tema de lusofonia em textos noticioso. De destacar as palavras Dilma, Rousseff e Brasil, o facto de, durante as datas de recolha das notícias (19 de março a 19 de maio de 2016) o caso Lava Jato estava em vogue e os principais intervenientes eram o Brasil e a Dilma Rousseff, daí sobressaírem-se essa palavras em relação a outras. 
