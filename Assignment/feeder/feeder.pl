#!/usr/bin/env perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

use utf8::all; # everything if utf8 \o/
use Data::Dumper;
use Text::Trim;
use HTML::Entities; # Translates html codes to plain text
use XML::RSS::Parser::Lite; # RSS XML Parser Lite
use LWP::Simple;
use HTML::TreeBuilder::XPath;

use syntax 'junction';
use autobox;
sub ARRAY::contains { any( @{$_[0]} ) eq $_[1] };

if ( !@ARGV ) { die "Usage: \n"; }

if (scalar @ARGV > 2 || scalar @ARGV == 1) { die "Usage: \n"; }

open (my $feedList, "<", "$ARGV[0]") or die("Impossible open file\n");
open (my $to_get_ids, "<", "$ARGV[1]") or die("Impossible open file\n");
open (my $news, ">>", "$ARGV[1]") or die("Impossible open file\n");

my @ids = ();

while(<$to_get_ids>) {
	my $line = $_;
	push @ids, $1 if $line =~ /<news id="([0-9]+)".*>/;
}

@ids = sort {$a <=> $b} @ids;

close($to_get_ids);

while(<$feedList>) {

	my $feed = $_;
	
	my $xml = get($feed);
	my $rpl = new XML::RSS::Parser::Lite;
	$rpl->parse($xml);

	my $category = clean_category($feed);

	for (my $i = 0; $i < $rpl->count(); $i++) {
		my $it = $rpl->get($i);

		my $guid = $it->get('guid');
		my $id = get_id($guid);

		next if @ids->contains($id);

		my $title = clean_title($it->get('title'));
		my $url = $it->get('url');
		my $description = clean_description($it->get('description'));
		my $pubdate = $it->get('pubDate');
		
		print $news "<news id=\"$id\" category=\"$category\">\n";
		print $news "<title>\n";
		print $news "$title\n";
		print $news "</title>\n";
		
		print $news "<description>\n";
		print $news "$description\n";
		print $news "</description>\n";

		print $news "<txt>\n";
		my $txt = get_url_full_text($guid);
		print $news "$txt\n";
		print $news "</txt>\n";

		print $news "<date>\n";
		print $news "$pubdate\n";
		print $news "</date>\n";

		print $news "<link>\n";
		print $news "$guid\n";
		print $news "</link>\n";

		print $news "</news>\n";
	}
}

close($news);
close($feedList);

sub get_id {
	my ($url) = shift @_;
	$url =~ /http\:\/\/diariodigital\.sapo\.pt\/news\.asp\?id_news\=([0-9]+)/;
	return $1;
}

sub clean_description {
	my ($desc) = shift @_;
	$desc =~ s/\R//g;
	$desc =~ s/&amp;#(\d+);/chr($1)/ge;
	my $desc_decoded = decode_entities($desc);
	return substr( $desc_decoded, 0, index ($desc_decoded, "<"));
}

sub clean_category {
	my ($cat) = shift @_;
	$cat =~ /http:\/\/[a-z.]+\/dd([A-Za-z]+)/;

	return $1;
}

sub clean_title {
	my ($t) = shift @_;
	$t =~ s/&amp;#(\d+);/chr($1)/ge;
	return $t;
}

sub get_url_full_text {

	my $res = "";
	my ($url) = shift @_;
	my $html = get $url;

	$html = replace("<br />", "</p><p>", $html);

	my $tree= HTML::TreeBuilder::XPath->new_from_content($html);

	my $nodes = $tree->findnodes('//div[@class="txt"]')->[0];

#	$res = $nodes->findvalue("h4");

#	trim $res;
	
	my @p = $nodes->findvalues("p");

	if (scalar @p == 1) {
		my $aux = "@p";
		$res .= trim($aux);
	} else {
		for my $string (@p) {
			my $final = trim($string);
			$final .= " ";
			$res .= $final;
		}
	}

	$tree->delete;
 
	return transform($res);
}

sub replace {
	my ($to_find, $to_replace, $txt) = @_;

	$txt =~ s/<br\ \/>/<\/p><p>/g;

	return $txt;
}

sub transform {

	my ($var) = shift @_;

	$var =~ s/([.!?])([A-Z])/$1 $2/g;
	$var =~ s/Diário\sDigital(\s)?(com|\/)(\s)?Lusa//g;
	return $var;
}
