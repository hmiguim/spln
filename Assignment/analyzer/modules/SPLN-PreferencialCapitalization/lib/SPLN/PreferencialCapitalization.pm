package SPLN::PreferencialCapitalization;

use 5.006;
use strict;
use utf8::all;
use warnings;
use DateTime;
use Lingua::Jspell;

=head1 NAME

SPLN::PreferencialCapitalization - The great new SPLN::PreferencialCapitalization!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use SPLN::PreferencialCapitalization;

    my $foo = SPLN::PreferencialCapitalization->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 new

=cut
sub new {
	my $class = shift;

	my $dictionary = Lingua::Jspell->new("pt");
	my $hash = { pct => {}, dictionary => $dictionary };

	return bless $hash, $class;
}

=head2 process

=cut
sub process {

	my ($self, $text) = @_;

	my $match = qr{\w+(-\w+)*};
	my $all_caps = qr{\G\b[A-Z]{2,}\b\Z};

	my %ocurrence;

	foreach (split /\n/, $text) {
	
		next if /^<(all|title|link|description|news|txt|date)/;
		next if /^<\//;
		next if /^http/;
		next if /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/;

		while( /$match/g ) {
			my $aux = $&;
			next if ($aux =~ /\d/);
			next if ( $aux =~ /$all_caps/ );
			
			$ocurrence{lc($aux)}{$aux}++;
		}
	}

	foreach my $lc_word (sort (keys %ocurrence)) {
		my $pref = _calculateCapitalization($ocurrence{$lc_word});

		if (defined $pref->{top_word}) {
			$self->{pct}{$lc_word} = $pref->{top_word};
		} else {
			my $preference = _heuristics($self->{dictionary},$lc_word);

			if (defined $preference) {
				$self->{pct}{$lc_word} = $preference;
			} else {
				$self->{pct}{$lc_word} = undef;
			}
		}
	}
}

=head2 preference

=cut
sub preference {
	my ($self, $text) = @_;

	return $self->{pct}{lc($text)};
} 

=head2 replace_non_greedy

=cut
sub replaceNonGreedy {
	my ($self, $text) = @_;

	my $_p_first_word = qr{(^\S+)\s*};
	my $_p_after_period = qr{(\.\s+?\S+)\s*};

	my $result = "";

	foreach my $line (split /\n/, $text) {

		if ($line =~ /^<(all|title|link|description|txt|news|date)/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^<\//) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^http/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		# Preferencially capitalizing the first word of each line;
		$line =~ s/$_p_first_word/_evaluate($self,$1)/eg;

		# Preferencially capitalizing the first word after a period;
		$line =~ s/$_p_after_period/_evaluate($self,$1)/eg;

		$result .= $line;

		$result .= "\n";
	}

	return $result;	
}

=head2 replace_greedy

=cut
sub replaceGreedy {
	my ($self, $text) = @_;

	my $match = qr{\w+(-\w+)*};

	my $result = "";

	foreach my $line (split /\n/, $text) {

		if ($line =~ /^<(all|title|link|description|txt|news|date)/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^<\//) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^http/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		if ($line =~ /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/) {
			$result .= $line;
			$result .= "\n";
			next;
		}

		foreach my $word (split /\s+/, $line) {

			my $w = $1 if ($word =~ /($match)/);
			
			if (defined $w) {
				if ($self->{pct}{lc($w)}) {
					my $to_replace = $self->{pct}{lc($w)};
					$word =~ s/$w/$to_replace/g;
				}		
			}

			$result .= $word . " "; 

		}

		$result .= "\n";
	}

	return $result;	
}

=head2 as_txt

=cut
sub asTxt {
	my $self = shift;
	
	my $delimiter = ",";
	my $dt = DateTime->now;
	my $date = $dt->ymd('');
	my $hour = $dt->hms('');
	my $filename = "$date$hour"."_output.txt";

	my $line = "";

	if (defined $_[0]) {
		my %params 	= @_;
		$delimiter 	= $params{delimiter}	if (defined $params{delimiter});	
		$filename 	= $params{filename} 	if (defined $params{filename});
	}

	foreach my $lc_word (sort (keys %{$self->{pct}})) {
			
		if (defined $self->{pct}{$lc_word}) {
			$line .= "$lc_word$delimiter$self->{pct}{$lc_word}\n";
		} else {
			$line .= "$lc_word$delimiter" . "UNDEF\n";
		}
	}

	open my $fh, ">:utf8", $filename or die "Can't create file: $!\n";
	print $fh $line;
	close $fh;
}

# PRIVATE SUBROUTINES/METHODS

sub _log10 {
	my $n = shift;
	return log($n)/log(10);
}

sub _isFirstUpperCased {
	my $word = shift;

	if ($word =~ /^[[:upper:]]/) {
		return 1;
	} 
	return 0;
}

sub _calculateCapitalization {

	my ($hash_line) = shift;

	my $stats = { top_word => undef, count => undef };

	my $total = scalar keys %{$hash_line};
	
	my $flag = 0; # if 1 the value comes from the hash $stats; if 0 the value comes from the hash hash_line;
	
	my $result = 0;
	
	my $i = 0;
	
	foreach my $word (sort {_ascending($hash_line->{$a},$hash_line->{$b})} keys %{$hash_line}) {
		$i++;
		if (defined $stats->{count}) {
			
			if ($stats->{count} > $hash_line->{$word}) {
				$result = _log10($stats->{count}) - _log10($hash_line->{$word});
				$flag = 1;
			} else {
				$result = _log10($hash_line->{$word}) - _log10($stats->{count});
				$flag = 0;
			}
			
			if ($result >= 0.5) {
				if (!$flag) {
					$stats = { top_word => $word, count => $hash_line->{$word}};
				}
				
			} else {
				if ($i == $total) {
					$stats = { top_word => undef, count => undef };
				}
			}
		} else {
			$stats = { top_word => $word, count => $hash_line->{$word}};
		}
	}

	return $stats;
}

sub _evaluate {
	my ($self,$impure_word) = @_;
	
	my $_p_word = qr{(\w+(-\w+)*)};
	my $pure_word = "";
	
	if ($impure_word =~ /$_p_word/) {
		$pure_word = $1;

		if (defined $pure_word) {
			my $pref = preference($self,$pure_word);

			if (defined $pref) {
				$impure_word =~ s/$pure_word/$pref/g;
			}
		}
	}

	return "$impure_word ";
}

sub _heuristics {
	my ($dictionary, $word) = @_;

	return lc($word) if _is_a_verb($dictionary, $word);

	return undef;
}

sub _is_a_verb {

	my ($dictionary, $word) = @_;

	my @list = $dictionary->fea($word);

	my $count = 0;

	my %hash = map { $_->{CAT} => ++$count } @list;

	if ($hash{"v"} && scalar keys %hash <= 1) {
		return 1;
	}	

	return 0;
}

sub _descending {

	my ($a,$b) = @_;

	if ($a < $b) {  return 1;  }
	if ($a > $b) {  return -1; }

	return $a == $b;
	
}

sub _ascending {

	my ($a,$b) = @_;

	if ($a > $b) {  return 1;  }
	if ($a < $b) {  return -1; }

	return $a == $b;
}

=head1 AUTHOR

Miguel Guimarães, C<< <hmtlguimaraes at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-spln-preferencialcapitalization at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=SPLN-PreferencialCapitalization>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc SPLN::PreferencialCapitalization


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=SPLN-PreferencialCapitalization>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/SPLN-PreferencialCapitalization>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/SPLN-PreferencialCapitalization>

=item * Search CPAN

L<http://search.cpan.org/dist/SPLN-PreferencialCapitalization/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Miguel Guimarães.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of SPLN::PreferencialCapitalization
