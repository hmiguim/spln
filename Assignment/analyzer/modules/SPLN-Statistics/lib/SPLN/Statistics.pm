package SPLN::Statistics;

use 5.006;
use strict;
use warnings;
use DateTime;

=head1 NAME

SPLN::Statistics - The great new SPLN::Statistics!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use SPLN::Statistics;

    my $foo = SPLN::Statistics->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 new

=cut

sub new {
	my $class = shift;
	my $hash = { freqs => {}, total => 0, type => "simple"  };

	return bless $hash, $class;
}

=head2 add

=cut

sub add {
	my ($self, $word) = @_;

	$self->{freqs}{$word}++;
	$self->{total}++;
}

=head2 info

=cut

sub info {
	my $self = shift;

	return "Unique Words: ", scalar keys %{$self->{freqs}}, "\n Word Count: ", $self->{total};
}


=head2 absoluteFrequency

=cut

sub absoluteFrequency {

	my ($self, $word) = @_;

	my $freq = $self->{freqs}{$word};

	if (defined $freq) {
		return $freq; 
	}

	return 0;
}

=head2 relativeFrequency

=cut

sub relativeFrequency {

	my ($self, $word) = @_;

	
	my $freq = $self->{freqs}{$word};

	if (defined $freq) {
		return ($freq/$self->{total})*1000000; 
	} 

	return 0;
}

=head2 logarithmFrequency

=cut

sub logarithmFrequency {

	my ($self, $word) = @_;

	
	my $freq = $self->{freqs}{$word};

	if (defined $freq) {
		return _log10(($freq/$self->{total})*1000000); 
	} 

	return 0;
}

=head2 relativeFrequencies

=cut

sub relativeFrequencies {

	my $self = shift;

	my $hash = { freqs => {}, total => 0, type => "simple" };

	foreach my $word (keys %{$self->{freqs}}) {
		my $r;
		$r = ($self->{freqs}{$word}/$self->{total})*1000000;
		$hash->{freqs}{$word} = $r;
		$hash->{total} += $r;
	}

	return bless $hash;
}

=head2 relativeFrequencies

=cut

sub absoluteFrequencies {

	my $self = shift;

	my $hash = { freqs => {}, total => 0, type => "simple"  };

	foreach my $word (keys %{$self->{freqs}}) {
		my $r = 0;
		$r = $self->{freqs}{$word};
		$hash->{freqs}{$word} = $r;
		$hash->{total} += $r;
	}

	return bless $hash;
}

=head2 frequencies

=cut

sub frequencies {

	my $self = shift;

	my $per = 1000000;

	if (defined $_[0]) {
		my %params 	= @_;
		$per		= $params{per}		if (defined $params{per});
	}

	my $hash = { freqs => {}, total => 0, type => "compose" };

	foreach my $word (keys %{$self->{freqs}}) {
		
		my $abs = $self->{freqs}{$word};
		my $rel = ($abs / $self->{total})*$per;
		my $log = _log10($rel);

		my $freqs = { absolute => $abs, relative => $rel, logarithm => $log };

		$hash->{total} += $abs;
		$hash->{freqs}{$word} = $freqs;
	}

	return bless $hash;
}

=head2 sum

=cut

sub sum {

	my ($self, @another) = @_;

	my $hash = { freqs => {}, total => 0, type => "simple" };

	foreach my $j (keys %{$self->{freqs}}) {
		if (defined $hash->{freqs}{$j}) {
			$hash->{freqs}{$j} += $self->{freqs}{$j};
			
		} else {
			$hash->{freqs}{$j} = $self->{freqs}{$j};
		}

		$hash->{total} += $self->{freqs}{$j};
	}

	foreach my $stats (@another) {
		foreach my $k (keys %{$stats->{freqs}}) {
			if (defined $hash->{freqs}{$k}) {
				$hash->{freqs}{$k} += $stats->{freqs}{$k};
			} else {
				$hash->{freqs}{$k} = $stats->{freqs}{$k};
			}

			$hash->{total} += $stats->{freqs}{$k};
		}
	}

	return bless $hash;

}

=head2 sum

=cut

sub subtract {

	my ($self, $stats) = @_;

	my $hash = { freqs => {}, total => 0, type => "simple" };

	foreach my $key (keys %{$self->{freqs}}) {
		if (defined $stats->{freqs}{$key}) {
			my $sub = ($self->{freqs}{$key}) - ($stats->{freqs}{$key});

			if ($sub > 0) {
				$hash->{freqs}{$key} = $sub;
				$hash->{total} += $sub;
			}
		} else {
			$hash->{freqs}{$key} = $self->{freqs}{$key};
			$hash->{total} += $self->{freqs}{$key};
		}
	}

	return bless $hash;

}

=head2 surprise

=cut

sub surprise {

	my ($self,$common) = @_;

	my $k = 0.5;

	my $hash = { freqs => {}, total => 0, type => "compose" };

	if (defined $_[0]) {
		my %params 	= @_;
		$k		= $params{k}		if (defined $params{k});
	}

	$self = $self->frequencies();
	$common = $common->frequencies();

	foreach my $key (keys %{$self->{freqs}}) {

		my $diff = $self->{freqs}{$key}->{logarithm} - $common->{freqs}{$key}->{logarithm};

		if ($diff > $k) {
			$hash->{freqs}{$key} = $self->{freqs}{$key};
			$hash->{total} += $self->{freqs}{$key}->{absolute};
		}
	}

	return bless $hash;

}
=head2 top

=cut

sub top {

	my ($self,$limit) = @_;
	my $hash = { freqs => {}, total => 0, type => "simple" };
	my $i = 1;

	$limit = 10 if (!(defined $limit));

	if ($self->{type} eq "simple") {
		foreach my $word (sort {_descending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			$hash->{freqs}{$word} = $self->{freqs}{$word};
			$hash->{total} += $self->{freqs}{$word};
			last if ($i++) == $limit;
		}
	}

	if ($self->{type} eq "compose") {
		foreach my $word (sort {_descending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			$hash->{freqs}{$word}->{absolute} = $self->{freqs}{$word}->{absolute};
			$hash->{total} += $self->{freqs}{$word}->{absolute};
			last if ($i++) == $limit;
		}

		foreach my $word (keys %{$hash->{freqs}}) {
			my $abs = $hash->{freqs}{$word}->{absolute};
			my $rel = ($abs / $hash->{total})*100;
			my $log = _log10($rel);

			$hash->{freqs}{$word}->{relative} = $rel;
			$hash->{freqs}{$word}->{logarithm} = $log;
		}

		$hash->{type} = "compose";
	}

	return bless $hash;
}

=head2 bottom

=cut

sub bottom {

	my ($self,$limit) = @_;
	my $hash = { freqs => {}, total => 0, type => "simple" };
	my $i = 1;

	$limit = 10 if (!(defined $limit));

	if ($self->{type} eq "simple") {
		foreach my $word (sort {_ascending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			$hash->{freqs}{$word} = $self->{freqs}{$word};
			$hash->{total} += $self->{freqs}{$word};
			last if ($i++) == $limit;
		}
	}

	if ($self->{type} eq "compose") {
		foreach my $word (sort {_ascending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			$hash->{freqs}{$word}->{absolute} = $self->{freqs}{$word}->{absolute};
			$hash->{total} += $self->{freqs}{$word}->{absolute};
			last if ($i++) == $limit;
		}

		foreach my $word (keys %{$hash->{freqs}}) {
			my $abs = $hash->{freqs}{$word}->{absolute};
			my $rel = ($abs / $hash->{total})*100;
			my $log = _log10($rel);

			$hash->{freqs}{$word}->{relative} = $rel;
			$hash->{freqs}{$word}->{logarithm} = $log;
		}

		$hash->{type} = "compose";
	}

	return bless $hash;
}

=head2 threshold

=cut

sub threshold {

	my ($self, $threshold) = @_;
	my $hash = { freqs => {}, total => 0, type => "simple" };

	if ($self->{type} eq "compose") {
		foreach my $word (sort {_descending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			if ($self->{freqs}{$word}->{absolute} >= $threshold) {
				$hash->{freqs}{$word}->{absolute} = $self->{freqs}{$word}->{absolute};
				$hash->{total} += $self->{freqs}{$word}->{absolute};
			}
		}

		$hash->{type} = "compose";
		
		foreach my $word (keys %{$hash->{freqs}}) {
			my $abs = $hash->{freqs}{$word}->{absolute};
			my $rel = ($abs / $hash->{total})*100;
			my $log = _log10($rel);

			$hash->{freqs}{$word}->{relative} = $rel;
			$hash->{freqs}{$word}->{logarithm} = $log;
		}

	}

	if ($self->{type} eq "simple") {
		foreach my $word (sort {_descending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {
			if ($self->{freqs}{$word} >= $threshold) {
				$hash->{freqs}{$word} = $self->{freqs}{$word};
				$hash->{total} += $self->{freqs}{$word};
			}
		}
	}

	return bless $hash;
}

=head2 cleanData

=cut

sub cleanData {

	my $self = shift;

	$self->{freqs} = {};
	$self->{total} = 0;
}

=head2 saveAsHtml

=cut

sub saveAsHtml {

	my $self = shift;
	my $filename = _filename_header('html');	
	my $order = "DESC";
	my $result = "";

	if (defined $_[0]) {
		my %params 	= @_;
		$filename 	= $params{filename} 	if (defined $params{filename});
		$order		= $params{order}		if (defined $params{order});
	}

	$result = _initialize_html($result, "Statistics Result");

		if ($self->{type} eq 'compose') {

			$result = _table_header_html($result, "Word", "Absolute Frequency", "Relative Frequency", "Logarithm Frequency");

			if (uc($order) eq 'ASC') {
				foreach my $word (sort {_ascending($a, $b, $self->{freqs}, "compose")} keys %{$self->{freqs}}) {		
					$result .= "<tr>\n<td>".$word."</td>\n<td>".$self->{freqs}->{$word}->{absolute}."</td>\n<td>".$self->{freqs}->{$word}->{relative}."</td>\n<td>".$self->{freqs}->{$word}->{logarithm}."</td></tr>\n";
				}
			}
	
			if (uc($order) eq 'DESC') {
				foreach my $word (sort {_descending($a, $b, $self->{freqs}, "compose")} keys %{$self->{freqs}}) {		
					$result .= "<tr>\n<td>".$word."</td>\n<td>".$self->{freqs}->{$word}{absolute}."</td>\n<td>".$self->{freqs}->{$word}{relative}."</td>\n<td>".$self->{freqs}->{$word}->{logarithm}."</td></tr>\n";
				}
			}
		} 

		if ($self->{type} eq 'simple') {
			$result = _table_header_html($result, "Word", "Frequency");

			if (uc($order) eq 'ASC') {
				foreach my $word (sort {_ascending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {		
					$result .= "<tr>\n<td>".$word."</td>\n<td>".$self->{freqs}{$word}."</td></tr>\n";
				}
			}
		
			if (uc($order) eq 'DESC') {
				foreach my $word (sort {_descending($a, $b, $self->{freqs}, $self->{type})} keys %{$self->{freqs}}) {		
					$result .= "<tr>\n<td>".$word."</td>\n<td>".$self->{freqs}{$word}."</td></tr>\n";
				}
			}
		}
	

	$result = _end_table_html($result);

	$result = _end_html($result);

	open my $fh, ">:utf8", $filename or die "Can't create file: $!\n";
	print $fh $result;
	close $fh;
}

# PRIVATE METHODS/SUBROUTINES

sub _log10 {
	my $n = shift;
	return log($n)/log(10);
}

sub _ascending {

	my ($a,$b,$hash,$type) = @_;
	
	if ($type eq "simple") {
		if ($hash->{$a} > $hash->{$b}) {  return 1;  }
		if ($hash->{$a} < $hash->{$b}) {  return -1; }

		return $a cmp $b;
	}
	
	if ($type eq 'compose') {
		if ($hash->{$a}->{absolute} > $hash->{$b}->{absolute}) {  return 1;  }
		if ($hash->{$a}->{absolute} < $hash->{$b}->{absolute}) {  return -1; }

		return $a cmp $b;
	}
}

sub _descending {

	my ($a,$b,$hash,$type) = @_;

	if ($type eq 'simple') {
		if ($hash->{$a} > $hash->{$b}) {  return -1; }
		if ($hash->{$a} < $hash->{$b}) {  return 1;	 }

		return $a cmp $b;
	}

	if ($type eq 'compose') {
		if ($hash->{$a}->{absolute} > $hash->{$b}->{absolute}) {  return -1; }
		if ($hash->{$a}->{absolute} < $hash->{$b}->{absolute}) {  return 1;	 }

		return $a cmp $b;
	}

	
}

sub _initialize_html {

	my ($html,$title) = @_;
	
	$html .= "<!DOCTYPE html>\n";
	$html .= "<html>\n";
	$html .= "<head>\n";
	$html .= "<meta charset=\"UTF-8\">\n";
	$html .= "<title>$title</title>\n";
	$html .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css\">\n";
	$html .= "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js\"></script>\n";
	$html .= "<script type=\"text/javascript\" src=\"http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js\"></script>\n";
	$html .= "</head>\n";
	$html .= "<body>\n";

	return $html;
}

sub _end_html {

	my $html = shift;

	$html .= "</body>\n";
	$html .= "<script>\$(document).ready(function(){\$('#table_results').DataTable();});</script>\n";
	$html .= "</html>";

	return $html;
}

sub _table_header_html {

	my ($html,@list) = @_;

	$html .= "<table id=\"table_results\">\n";
	$html .= "<thead>\n";
	$html .= "<tr>\n";

    for my $t (@list) {
    	$html .= "<th>$t</th>\n";
    }

	$html .= "</tr>\n";
	$html .= "</thead>\n";
	$html .= "<tbody>\n";

	return $html;
}

sub _end_table_html {
	my $html = shift;

	$html .= "</tbody>\n";
	$html .= "</table>\n";
}

sub _filename_header {
	
	my $type = shift;

	my $dt = DateTime->now;
	my $date = $dt->ymd('');
	my $hour = $dt->hms('');

	return "$date$hour"."_output."."$type";
}

=head1 AUTHOR

Miguel Guimarães, C<< <hmtlguimaraes at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-spln-statistics at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=SPLN-Statistics>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc SPLN::Statistics


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=SPLN-Statistics>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/SPLN-Statistics>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/SPLN-Statistics>

=item * Search CPAN

L<http://search.cpan.org/dist/SPLN-Statistics/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Miguel Guimarães.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of SPLN::Statistics
