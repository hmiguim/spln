#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'SPLN::Statistics' ) || print "Bail out!\n";
}

diag( "Testing SPLN::Statistics $SPLN::Statistics::VERSION, Perl $], $^X" );
