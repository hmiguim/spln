package SPLN::Analyzer;

use 5.006;
use strict;
use warnings;
use utf8::all;
use Lingua::Jspell;

use Array::Utils qw(:all);

use lib '../SPLN-Statistics/lib';
use SPLN::Statistics;

=head1 NAME

SPLN::Analyzer - The great new SPLN::Analyzer!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use SPLN::Analyzer;

    my $foo = SPLN::Analyzer->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 new

=cut
sub new {

	my $class = shift;

	my $dictionary = Lingua::Jspell->new("pt");
	my $hash = { class => {}, dictionary => $dictionary, quarantine => {} };
	
	return bless $hash, $class;
}

sub words {
	my ($self,$text) = @_;

	my $stats = SPLN::Statistics->new();

	my $match = qr{\w+(-\w+)*};

	foreach (split /\n/, $text) {
	
		next if /^<(all|title|link|description|news|txt|date)/;
		next if /^<\//;
		next if /^http/;
		next if /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/;

		while( /\b$match\b/g ) {
			my $aux = $&;
			next if ($aux =~ /\d/);
			$stats->add(lc($aux));
		}
	}

	return $stats;
}

=head2 findEntities

=cut
sub findEntities {

	my ($self,$text) = @_;
	my $stats = SPLN::Statistics->new();

	my $pm = qr{[[:upper:]]\w+};
	my $prep = qr{d[eoa]s?};
	my $np = qr{$pm(( )?(-)?($prep )?$pm)*};

	foreach (split /\n/, $text) {

		next if /^<(all|title|link|description|txt|news|date)/;
		next if /^<\//;
		next if /^http/;
		next if /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/;

		while( /$np/g ) { 
			my $word = $&;

			if (_isEntity($word, $self->{dictionary})) {
				$stats->add($&);
			} else {
				$self->{quarantine}{Analyzer}{$&}++;
			}
			
		}
	}

	return $stats;

} 

=head2 findAcronyms

=cut
sub findAcronyms {

	my ($self,$text) = @_;
	my $stats = SPLN::Statistics->new();

	my $pn = qr{([[:upper:]]\w+)};
	my $prepositions = qr{(d[eoa]s?|para|em|sobre|[àaeo]s?)};
	
	my $acro = qr{($pn(( $prepositions)?(-|\s)$pn)+) (\([A-Z]+\))};

	foreach (split /\n/, $text) {

		next if /^<(all|title|link|description|news|txt|date)/;
		next if /^<\//;
		next if /^http/;
		next if /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/;

		while( /$acro/g ) {
			if (_isAcronym($1,$&)) {
				$stats->add($&);
			} else {
				$self->{quarantine}{acro}{$&}++;
			}
		}
	}

	return $stats;

} 

=head2 classifier

=cut
sub classifier {

	my ($self,$text) = @_;
	my $category = "";

	my $match = qr{\w+(-\w+)*};

	my $hash = { class => {} };

	foreach (split /\n/, $text) {

		if (/<news id="[0-9]+" category="([A-Za-z]+)">/) {
			$category = $1;
			next;
		}

		next if /^<(all|title|link|description|txt|news|date)/;
		next if /^<\//;
		next if /^http/;
		next if /^Mon|Tue|Wed|Thu|Fri|Sat|Sun/;

		while( /\b$match\b/g ) {

			my $aux = $&;
			next if ($aux =~ /\d/);
			next if ($aux =~ /ª|º/);

			my $stats = $self->{class}{$category};

			if (! defined $stats) {
				$stats = SPLN::Statistics->new();
			}
			
			$stats->add(lc($aux));
			$self->{class}{$category} = $stats;			
		}
	}

	my $common = SPLN::Statistics->new();

	foreach my $key (keys %{$self->{class}}) {
		$common = $common->sum($self->{class}{$key});
	}

	foreach my $key (keys %{$self->{class}}) {
		my $result = $self->{class}{$key}->surprise($common);

		$hash->{class}{$key} = $result;
	} 

	return $hash;
} 

# PRIVATE METHODS/SUBROUTINES
sub _isAcronym {

	my ($t,$full) = @_;
	my $prepositions = qr{(d[eoa]s?|para|em|sobre|[àaeo]s?)};
	
	$full =~ /\(([A-Z]+)\)/;

	my $acro = $1;

	$t =~ s/Trás-os-Montes/TrásosMontes/g; # SPECIAL CASES
	$t =~ s/Pós-Graduação/Graduação/g; # SPECIAL CASES
	$t =~ s/\b$prepositions\b//g;
	$t =~ s/-/ /g;
	
	if ((length $acro) == _count_words($t)) { # First Test: checks if the number of letters in the acro are the same of number words in the expanding version 
		
		my $initials = _get_initials($acro); # ex: DGV - HASH(0 => "D", 1 => "G", 2 => "V") 

		my $acro_count = scalar keys %{$initials}; # number of letters in the acronym
		my $t_count = 0; # number of matches
		my $i=0;
		foreach my $word (split /\s+/, $t) { # split the sentence in words (by spaces)
			my $char = uc(substr $word, 0, 1); # upper case the first letter of each word
			if ($initials->{$i} eq $char) { # checks if the letter in the position $i is the same that $char
				$t_count++;
			}
			$i++;
		}

		if ($acro_count == $t_count) {
			return 1;
		}
	}

	return 0;

}

sub _get_initials {
	my $acro = shift;

	my %initials;
	my $i = 0;
	foreach my $char (split //, $acro) {
		$initials{$i++} = $char;
	}

	return \%initials;
}

sub _isEntity {
	my ($text, $dictionary) = @_;

	if (_count_words($text) > 1) {
		return 1;
	}
	
	my @list = $dictionary->fea($text);

	foreach my $analyze (@list) {
		if ($analyze->{CAT} eq 'np') {
			return 1;
		}
	}

	return 0;
}

sub _count_words {
	my $text = shift;
	my $num; 
    $num++ while $text =~ /\S+/g;
    return $num;
}

=head1 AUTHOR

Miguel Guimarães, C<< <hmtlguimaraes at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-spln-Analyzer at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=SPLN-Analyzer>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc SPLN::Analyzer


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=SPLN-Analyzer>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/SPLN-Analyzer>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/SPLN-Analyzer>

=item * Search CPAN

L<http://search.cpan.org/dist/SPLN-Analyzer/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Miguel Guimarães.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of SPLN::Analyzer
