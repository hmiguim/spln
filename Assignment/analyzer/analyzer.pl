#!/usr/bin/env perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

use utf8::all; # everything if utf8 \o/
use Data::Dumper;
$Data::Dumper::Useperl = 1; # print UTF-8
use Array::Utils qw(:all);

use Lingua::Jspell;

use lib 'modules/SPLN-Statistics/lib';
use lib 'modules/SPLN-PreferencialCapitalization/lib';
use lib 'modules/SPLN-Analyzer/lib';
use SPLN::Analyzer;
use SPLN::Statistics;
use SPLN::PreferencialCapitalization;

my $text = "";

{
	open (my $file, "<:utf8", "$ARGV[0]") or die("Impossible open file\n");
	local $/ = undef;
 	$text = <$file>;
	close $file;
}

my $pct = SPLN::PreferencialCapitalization->new(); 

$pct->process($text);

#print $pct->preference("interno"), "\n";
#

my $text_non_greedy = $pct->replaceNonGreedy($text);


#my $text_greedy = $pct->replaceGreedy($text);

my $analyzer = SPLN::Analyzer->new();
my $result = $analyzer->classifier($text_non_greedy);

my $class_auto = $result->{class}{Auto};
my $class_desporto = $result->{class}{Desporto};
my $class_luso = $result->{class}{Lusofonia};
my $class_multimedia = $result->{class}{Multimedia};
my $class_politica = $result->{class}{Politica};
my $class_pessoas = $result->{class}{Pessoas};
my $class_saude = $result->{class}{Saude};
my $class_sociedade = $result->{class}{Sociedade};


print "\nAuto\n";
print $class_auto->threshold(50)->info();
print "\nDesporto\n";
print $class_desporto->threshold(50)->info();
print "\nLusofonia\n";
print $class_luso->threshold(50)->info();
print "\nMultimedia\n";
print $class_multimedia->threshold(50)->info();
print "\nPolitica\n";
print $class_politica->threshold(50)->info();
print "\nPessoas\n";
print $class_pessoas->threshold(50)->info();
print "\nSaude\n";
print $class_saude->threshold(50)->info;
print "\nSociedade\n";
print $class_sociedade->threshold(50)->info;
print "\n";

$class_auto->threshold(50)->saveAsHtml(filename => "auto-key-words.html");
$class_desporto->threshold(50)->saveAsHtml(filename => "desporto-key-words.html");
$class_luso->threshold(50)->saveAsHtml(filename => "luso-key-words.html");
$class_multimedia->threshold(50)->saveAsHtml(filename => "multimedia-key-words.html");
$class_politica->threshold(50)->saveAsHtml(filename => "politica-key-words.html");
$class_pessoas->threshold(50)->saveAsHtml(filename => "pessoas-key-words.html");
$class_saude->threshold(50)->saveAsHtml(filename => "saude-key-words.html");
$class_sociedade->threshold(50)->saveAsHtml(filename => "sociedade-key-words.html");

#print Dumper($analyzer->{class});



#$analyzer->words($text_non_greedy)->frequencies->save_as_html();

__END__

#my $stats = $analyzer->findAcronyms($text_non_greedy);

#print $stats->info, "\n";
#$stats->frequencies->save_as_html(filename => "acros_non_greedy.html");
#$analyzer->findEntities($text_greedy)->save_as_html(filename => "entities_greedy.html");

#$analyzer->findAcronyms($text_non_greedy)->save_as_html(filename => "acros_non_greedy.html");
#$analyzer->findAcronyms($text_greedy)->save_as_html(filename => "acros_greedy.html");


my $classifier = $analyzer->classifier($text_non_greedy);

foreach my $key (keys %{$classifier}) {

	my @list = @{$classifier->{$key}};
	
	print "****************************** \n";
	print "\t", uc($key), "\n";
	print "****************************** \n";
	foreach my $item (@list) {
		print $item, "\n";
	}

	
}

__END__
$analyzer->{class}->{"Lusofonia"}->top(50)->frequencies->save_as_html(filename => "lusofonia.html");
$analyzer->{class}->{"Multimedia"}->top(50)->frequencies->save_as_html(filename => "multimedia.html");
$analyzer->{class}->{"Mundo"}->top(50)->frequencies->save_as_html(filename => "mundo.html");
$analyzer->{class}->{"Pessoas"}->top(50)->frequencies->save_as_html(filename => "pessoas.html");
$analyzer->{class}->{"Politica"}->top(50)->frequencies->save_as_html(filename => "politica.html");
$analyzer->{class}->{"Saude"}->top(50)->frequencies->save_as_html(filename => "saude.html");
$analyzer->{class}->{"Sociedade"}->top(50)->frequencies->save_as_html(filename => "sociedade.html");
