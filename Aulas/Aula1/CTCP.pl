#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;
use Data::Dumper;

$Data::Dumper::Useperl = 1; # print UTF-8

my (%oco);
my (%ratio);
my ($c);

my $pm = qr{[[:upper:]]\w+};
my $prep = qr{d[eoa]s?};
my $np = qr{$pm( ($prep )?$pm)*};

while(<>) {

	next if /^</;
	$_ =~ s/^\w+(-\w+)*//;
 	while( /\w+(-\w+)*/g ) { 
		$oco{lc($&)}{$&}++;
	}
}

foreach my $word (keys %oco) {

	foreach my $score (keys %oco->{$word}) {
		print "$oco->{$word}{$score}\n";
	}
	
}

__END__
#binmode STDOUT, 'utf8';
print Dumper(\%oco)

__END__
print "$c palavras\n";

foreach my $p (sort{ mycompare($a,$b)} keys %oco) {
	print "$p: $oco{$p}\n";
}

sub mycompare {
	my($a,$b)=@_;

	if ($oco{$a} > $oco{$b}) {return -1};
	if ($oco{$a} < $oco{$b}) {return 1};
	return $a cmp $b;
}

# $a <=> $b - crescente
# $b <=> $a - decrescente

# %a - array associativo -- tabela de hash -- chave pode ser qualquer coisa 
