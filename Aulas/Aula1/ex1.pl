#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;

my (%oco);

my ($c);

my $pm = qr{[[:upper:]]\w+};
my $prep = qr{d[eoa]s?};
my $np = qr{$pm( ($prep )?$pm)*};

while(<>) {
	
	next if /^</;
 	while( /$np|\w+(-\w+)*/g ) { # palavras hifenizadas
		$oco{$&}++;
		$c++;
	}
}

print "$c palavras\n";

foreach my $p (sort{ mycompare($a,$b)} keys %oco) {
	print "$p: $oco{$p}\n";
}

sub mycompare {
	my($a,$b)=@_;

	if ($oco{$a} > $oco{$b}) {return -1};
	if ($oco{$a} < $oco{$b}) {return 1};
	return $a cmp $b;
}

# $a <=> $b - crescente
# $b <=> $a - decrescente

# %a - array associativo -- tabela de hash -- chave pode ser qualquer coisa 
