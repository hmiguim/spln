#!/usr/bin/perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

# use utf8::all; # everything is utf8 \o/
use FL3 'pt';
use Data::Dumper;
$Data::Dumper::Useperl = 1; # print UTF-8

my $texto;

{
	open (my $file, "<:utf8", "$ARGV[0]") or die("Impossible open file\n");
	local $/ = undef;
 	$texto = <$file>;
	close $file;
}

binmode STDOUT, ':utf8';

# atomizacao
my $tokens = tokenizer->tokenize($texto);
# segmentacao
my $frases = splitter->split($tokens);

# analise morfologica
$frases = morph->analyze($frases);

# Etiquetacao POS (Hidden Markov Models)
$frases = relax->tag($frases);

for my $f (@$frases) {
  my @words = $f->words();
  for my $w (@words) {
    my $h = $w->as_hash();
    print join("\t", map { $h->{$_} } (qw.form lemma tag.)), "\n";
  }
}

# chart parser

#$frases = chart->parse($frases);
#
#for my $f (@$frases) {
#	print "### ",$f->to_text,"\n";
#
#	if ($f->is_parsed) {
#		my $parseTree = $f->parse_tree;
#		print $parseTree->dump;
#	}
#}

$frases = nec->analyze($frases);

