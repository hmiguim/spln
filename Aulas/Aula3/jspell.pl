#!/usr/bin/env perl

use warnings;
use strict;
use utf8::all;
use Lingua::Jspell;
use Data::Dumper;

my $dic = Lingua::Jspell->new("pt");

# my @analysis = $dic->fea("era");

my @forms = $dic->fea("ser");

print Dumper(\@forms);
