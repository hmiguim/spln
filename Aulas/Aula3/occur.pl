#!/usr/bin/perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

use utf8::all; # everything if utf8 \o/
use Lingua::Jspell;

use Memoize;
memoize('radicais');

my $dic = Lingua::Jspell->new("pt");

my (%oco, $c);

while (<>) {
  next if /^</; # ignore tags

  while( /\w+(-\w+)*/g ) {
   
    my $word = $&;

    my @rad = radicais($word);
    for my $rad (@rad) {
      $oco{$rad}++;
    }
  }
}

for my $rad (sort { $oco{$b} <=> $oco{$a} } keys %oco) {
   print "$rad\t$oco{$rad}\n"; 
}

sub radicais {
  my ($word) = @_; # $word = shift @_ (remove primeiro elemento)
  return $dic->rad($word);
}
