#!/usr/bin perl

use warnings;
use strict;
use utf8;
use Data::Dumper;

use lib 'lib';
use Statistics::Frequencies;

my $freqs = Statistics::Frequencies->new();

$freqs->add("cavalo");
$freqs->add("cavalo","alado");
$freqs->add("cavalo","branco","de","napoleão");
$freqs->add("cavalo");

$freqs->save("freqs.dat");

print $freqs->types;
print "\n";
print $freqs->forms;
print "\n";
