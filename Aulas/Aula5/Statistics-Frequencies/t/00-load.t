#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

BEGIN {
	plan tests => 6;
    use_ok( 'Statistics::Frequencies' ) || print "Bail out!\n";
}

my $table = Statistics::Frequencies->new();

ok $table, "Tabela vazia";

is ref($table), "Statistics::Frequencies", "tem o tipo certo";

is_deeply $table, { freqs => {} }, "Tem estrutura certa";

$table->add("palavra");
ok exists($table->{freqs}{palavra});
is $table->{freqs}{palavra}, 1;

diag( "Testing Statistics::Frequencies $Statistics::Frequencies::VERSION, Perl $], $^X" );

# done_testing();
