#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;

use Data::Dumper;
$Data::Dumper::Useperl = 1; # print UTF-8
use Lingua::Jspell;

use Geo::GeoNames;
my $geo = new Geo::GeoNames(username => "hmtlg");

my $dictionary = Lingua::Jspell->new("pt");

my $pm = qr{[[:upper:]]\w+};

my $prepositions = qr{em|n[ao]s? };
my %hash;

while(<>) {

	while (/(\w+ )?<EM N=\"([0-9]+)\">([^<]*)<\/EM>/g) {
		if (defined $1) {
			$hash{$2} = isPerson($1,$3);
		} else {
			$hash{$2} = isPerson($3);
		}
		
	}
}

foreach my $key (sort {$a <=> $b} keys %hash) {
	print $key , "\t", $hash{$key}, "\n";
}

sub isPerson() {
	my ($before, $entity) = @_;

	if (defined $entity) {
		my @list = $dictionary->fea($before);
		foreach my $analyze (@list) {
			if ($analyze->{CAT} eq 'v') {
				return "PESSOA";
			}
		}

		@list = $dictionary->fea($entity);
		foreach my $analyze (@list) {
			if ($analyze->{SEM}) {
				if ($analyze->{SEM} eq 'country' || $analyze->{SEM} eq 'cid') {
					return "LOCAL";
				}
				
			}
		}

		my $result = $geo->search(q => $entity, maxRows => 2);

		if (scalar @$result > 1) {
			return "LOCAL";
		}


		if ($before =~ /por/) {
			return "PESSOA";
		}

		if (lc($before) =~ /$prepositions/) {
			return "LOCAL";
		}

		if (lc($entity) =~ /^rua/) {
			return "LOCAL";
		}

		if (lc($entity) =~ /^estado/) {
			return "LOCAL";
		}

	} else {
		my @list = $dictionary->fea($before);
		foreach my $analyze (@list) {
			if ($analyze->{SEM}) {
				if ($analyze->{SEM} eq 'country' || $analyze->{SEM} eq 'cid') {
					return "LOCAL";
				}
				
			}
		}

		my $result = $geo->search(q => $before, maxRows => 2);

		if (scalar @$result > 1) {
			return "LOCAL";
		}

		if (lc($before) =~ /^rua/) {
			return "LOCAL";
		}

		if (lc($before) =~ /^estado/) {
			return "LOCAL";
		}

	}

	return "PESSOA";
}
