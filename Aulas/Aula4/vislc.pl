#!/usr/bin/perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

use utf8::all; # everything if utf8 \o/
use Lingua::Jspell;
use Memoize;
use Data::Dumper;

my $dict = Lingua::Jspell->new("pt");

while (<STDIN>) {

  while (/\w+(-\w+)*|[.,:;?!]/g) {
    my $word = $&;

    my @l = $dict->fea($word);

    print "<\"$word\">\n";

    for my $analyze (@l) {
      print "\t$analyze->{rad}\t$analyze->{CAT}";
      delete $analyze->{rad};
      delete $analyze->{CAT};
      for my $key (keys %$analyze) {
        print "\t$key=$analyze->{$key}";
      }
      print "\n";
    }
  }
}
