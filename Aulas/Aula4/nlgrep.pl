#!/usr/bin/perl

use warnings; # Semantic errors are caught by the compiler
use strict;   # Strictly require variables declarations and other sutff

use utf8::all; # everything if utf8 \o/
use Lingua::Jspell;
use Memoize;
use Data::Dumper;

my @patterns;
my $dict = Lingua::Jspell->new("pt");
my $len;

if ( !@ARGV ) { die "No arguments provided"; }

@patterns = map { parse_argv($_)} @ARGV;

$len = @patterns; # Get the lenght 
#foreach my $elem(@ARGV) {
#  push(@patterns, parse_argv($elem));
# } 

@ARGV = ();

while (<STDIN>) {

  my @slidingwindow = map {""} @patterns;
  # my @slidingwindow = ("") x $len;
  my $line = $.;
  while (/\w+(-\w+)*|[.,:;?!]/g) {
    my $word = $&;
    push @slidingwindow, $word;
    shift @slidingwindow;
    
    if (multiwordmatch(\@slidingwindow)) {
      print "($line @slidingwindow) $_\n";
    }
  }

}

sub parse_argv {
  my ($arg) = @_;
  my %f = split(/[:,]/, $arg);

  return \%f;
}

sub multiwordmatch {
  my ($window) = @_;

  for my $i (0..$len-1) {
    if (!$dict->fea($window->[$i], $patterns[$i])) {
      return 0;
    }

    return 1;
  }

}
