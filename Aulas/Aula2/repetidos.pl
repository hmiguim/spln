#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;

my %lines;

while(<>) {
	if(/(\w+)\s+(.+)/) {
		if($lines{$1}) {
			print "$lines{$1} = $2\n";
		} else {
			$lines{$1} = $2;
		}
	}
}