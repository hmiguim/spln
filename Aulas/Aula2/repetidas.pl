#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;

my %lines;

while(<>) {
	if($_=~/\S/ && $lines{$_}) {
		print $_;
	} else {
		$lines{$_}++;
	}
}