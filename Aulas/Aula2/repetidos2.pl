#!/usr/bin/perl

use warnings;
use strict;
use utf8::all;

my %lines;
# my $arg = join( " ", @ARGV);
my $arg = join( " ", map{-d $_?"$_/*":$_}@ARGV);

open (P, "-|", "md5sum $arg") or die("Impossible open file\n");

while(<P>) {
	if(/(\w+)\s+(.+)/) {
		if($lines{$1}) {
			print "$lines{$1} = $2\n";
		} else {
			$lines{$1} = $2;
		}
	}
}
